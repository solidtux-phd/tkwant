:mod:`tkwant.integration` -- Fixed-order integration quadratures
================================================================

.. module:: tkwant.integration
.. autosummary::
    :toctree: generated

    calc_abscissas_and_weights
